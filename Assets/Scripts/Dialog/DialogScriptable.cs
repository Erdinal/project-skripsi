﻿using UnityEngine;

[CreateAssetMenu(fileName = "DialogData", menuName = "DialogData", order = 3)]
public class DialogScriptable : ScriptableObject
{
    [Header("Dialog Data")]

    public bool startDialog;

    public string[] dialogAvatarName;

    public Sprite[] dialogAvatarSprite;

    [TextArea]
    public string[] dialogText; 
}
