﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialog : MonoBehaviour
{
    private bool alreadyTrigerred = false;
    public string targetGameObjectName;
    public DialogManager dialogManager;   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.name == targetGameObjectName && alreadyTrigerred == false)
        {
            dialogManager.GetComponent<DialogManager>().triggerNextDialog();
            alreadyTrigerred = true;
        }
    }
}
