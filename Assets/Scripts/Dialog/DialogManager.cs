﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogManager : MonoBehaviour
{
    [Header("Helper")]
    private int indexDialogData;
    public Animator dialogAnimator;
    private GameManager gameManager;

    [Header("Dialog Data")]
    public DialogScriptable[] dialogData;

    [Header("Dialog Helper Object")]
    public Image avatarSpriteUI;
    public TextMeshProUGUI avatarnameTextUI;
    public TextMeshProUGUI dialogTextUI;

    private bool startDialog;
    private Queue<Sprite> avatarSprite;
    private Queue<string> avatarName;
    private Queue<string> dialogueText;

    [Header("Assign the value for the last level")]
    public SceneManagement sceneManagement;

    private void Awake()
    {
        gameManager = GameObject.Find("==GAMEMANAGER ==").GetComponent<GameManager>();
        indexDialogData = 0;
        avatarSprite = new Queue<Sprite>();
        avatarName = new Queue<string>();
        dialogueText = new Queue<string>();
    }

    void Start()
    {
        if(dialogData.Length != 0)
        {
            getDialogData(indexDialogData);
            if (startDialog)
            {
                dialogAnimator.SetTrigger("startDialog");
                ShowDialog();
            }
        }
    }

    private void Update()
    {
        if (gameManager.dialoguePopUp)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ShowDialog();
            }
        }
    }

    public void ShowDialog()
    {
        gameManager.dialoguePopUp = true;
        if (dialogueText.Count != 0)
        {
            avatarSpriteUI.sprite = avatarSprite.Dequeue();
            avatarnameTextUI.text = avatarName.Dequeue();
            dialogTextUI.text = dialogueText.Dequeue();
        }
        else
        {
            dialogAnimator.ResetTrigger("startDialog");
            dialogAnimator.SetTrigger("endDialog");
            gameManager.dialoguePopUp = false;

            // for trigger end game panel
            if(sceneManagement != null && indexDialogData == (dialogData.Length - 1))
            {
                sceneManagement.EndGame();
            }
        }
    }

    private void getDialogData(int index)
    {
        avatarSprite.Clear();
        avatarName.Clear();
        dialogueText.Clear();

        DialogScriptable data = dialogData[index];
   
        for (int i = 0; i < data.dialogText.Length; i++)
        {
            startDialog = data.startDialog;
            avatarSprite.Enqueue(data.dialogAvatarSprite[i]);
            avatarName.Enqueue(data.dialogAvatarName[i]);
            dialogueText.Enqueue(data.dialogText[i]);
        }
        
    }

    public void triggerNextDialog()
    {
        bool awakeDialog = false;

        if (dialogData.Length != 1 && dialogData[0].startDialog)
        {
            awakeDialog = true;
            indexDialogData++;
        }
        getDialogData(indexDialogData);
        dialogAnimator.ResetTrigger("endDialog");
        dialogAnimator.SetTrigger("startDialog");
        ShowDialog();

        if(!awakeDialog)
            indexDialogData++;
    }
}
