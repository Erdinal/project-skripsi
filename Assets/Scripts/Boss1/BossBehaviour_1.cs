﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BossBehaviour_1 : MonoBehaviour
{
    public EnemyBehaviour enemyBehaviour;

    [Header("Projectile")]
    public GameObject[] projectile;
    public Transform projectileSpawnPoint;
    private int projectileCount;

    [Header("Projectile Time Spawn")]
    public float[] timeIntervalProjectile;
    private float time_1 = 0f;

    void Start()
    {
        projectileCount = 0;
    }

    void Update()
    {
        SpawnProjectile();
    }

    void SpawnProjectile()
    {
        time_1 += Time.deltaTime;

        if (time_1 > timeIntervalProjectile[0])
        {
            GameObject mProjectile = Instantiate(projectile[0], projectileSpawnPoint.position, 
                projectileSpawnPoint.rotation);

            if(projectileCount == 5)
            {
                mProjectile.GetComponent<ProjectileBehaviour>().canBePunched = true;
                mProjectile.transform.localScale = new Vector3(2.5f, 2.5f, 0);
                projectileCount = 0;
            }

            time_1 = 0f;
            projectileCount++;
        }
    } 
}
