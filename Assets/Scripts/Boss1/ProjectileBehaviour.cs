﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    Vector2 direction;

    public float speed;

    [HideInInspector]
    public bool canBePunched;
    [HideInInspector]
    public bool isPunched;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isPunched = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPunched)
        {
            direction = new Vector2(-1 * speed, rb.velocity.y);
        }
        else
        {
            direction = new Vector2(1 * speed, rb.velocity.y);
        }
        
        rb.velocity = direction;
    }

    public void Punched()
    {
        if (canBePunched)
        {
            isPunched = true;
        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.name == "Player")
        {
            col.gameObject.GetComponent<PlayerBehaviour>().TakeDamage(10, 0);
            Destroy(gameObject);
        }

        if(col.gameObject.tag == "Projectile")
        {
            if(canBePunched == true)
            {
                Destroy(col.gameObject);
            }
        }

        if (col.gameObject.name == "MESIN KAYU")
        {
            if(isPunched == true)
            {
                col.gameObject.GetComponent<EnemyBehaviour>().TakeDamage(20);
                Destroy(gameObject);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }
    }
}
