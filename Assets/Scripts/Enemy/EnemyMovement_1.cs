﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement_1 : MonoBehaviour
{
    [Header("Enemy Attribute")]
    public SoundManager soundManager;
    public EnemyAttribute enemyAttribute;
    public LayerMask targetLayerMask;
    public EnemyBehaviour enemyBehaviour;

    // attribute
    private float speed;
    private float lookRadius;
    public Rigidbody2D rb;
    public Transform attackPoint;

    // player variabel
    public Transform playerTransform;

    // variabel
    [HideInInspector] public bool facingRight;
    [HideInInspector]
    public bool isHitted = false;

    // Animation
    [Header("Animation")]
    public Animator animator;

    // AttackAttribute
    [Header("AttackAttribute")]
    public float attackInterval;
    private float attackTime;
    private bool isAttack = false;

    void Start()
    {
        speed = enemyAttribute.movementSpeed;
        lookRadius = enemyAttribute.lookRadius;
        facingRight = false;
        attackTime = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!enemyBehaviour.die)
        {
            Collider2D[] cols = Physics2D.OverlapCircleAll(new Vector2(
            GetComponent<Transform>().position.x, GetComponent<Transform>().position.y), lookRadius,
            targetLayerMask);

            Collider2D[] colsAttackRange = Physics2D.OverlapCircleAll(new Vector2(
                attackPoint.position.x, attackPoint.position.y), .25f,
                targetLayerMask);

            if (cols.Length == 0)
            {
                animator.SetBool("isWalk", false);
            }

            if (cols.Length > 0 && colsAttackRange.Length < 1)
            {

                if (!cols[0].GetComponent<PlayerBehaviour>().die)
                {
                    animator.SetBool("isWalk", true);
                    if (GetComponent<Transform>().position.x < playerTransform.position.x && !isAttack && !isHitted)
                    {
                        Move(1);
                    }

                    if (GetComponent<Transform>().position.x > playerTransform.position.x && !isAttack && !isHitted)
                    {
                        Move(-1);
                    }

                    if (isAttack || isHitted)
                    {
                        Move(0);
                    }
                }
                else
                {
                    Move(0);
                }
                    
            }

            // ATTACK
            attackTime += Time.deltaTime;

            if (colsAttackRange.Length > 0)
            {
                if (attackTime >= attackInterval)
                {
                    isAttack = true;
                    if (!colsAttackRange[0].GetComponent<PlayerBehaviour>().die)
                    {
                        Attack(colsAttackRange[0]);
                    }
                    
                }
            }
        }
        else
        {
            animator.ResetTrigger("attack");
            animator.SetBool("isWalk", false);
            Move(0);
        }
    }

    public void Move(float xDirection)
    {
        if(facingRight && xDirection == -1)
        {
            transform.Rotate(new Vector3(0, -180, 0));
            facingRight = false;
        }

        if(!facingRight && xDirection == 1)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            facingRight = true;
        }   

        Vector2 direction = new Vector2(xDirection * speed, rb.velocity.y);
        rb.velocity = direction;
    }

    void Attack(Collider2D col)
    {
        attackTime = 0f;
        soundManager.PlayActionSound(0);
        animator.SetTrigger("attack");
        animator.SetBool("isWalk", false);

        float knockbackDirection = 0f;

        if (facingRight)
        {
            knockbackDirection = 1500f;
        }

        if (!facingRight)
        {
            knockbackDirection = -1500f;
        }

        col.GetComponent<PlayerBehaviour>().TakeDamage(10, knockbackDirection);
        isAttack = false;
    }
    

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(GetComponent<Transform>().position, 5f);

        Gizmos.DrawWireSphere(attackPoint.position, .5f);
    }

}
