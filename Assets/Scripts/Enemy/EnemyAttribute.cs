﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "EnemyData", order = 2)]
public class EnemyAttribute : ScriptableObject
{
    [Header("Attribute")]
    public int health;
    [Range(0f, 5f)]
    public float movementSpeed;
    [Range(0.1f, 5f)]
    public float lookRadius;

    public bool isBoss;
}
