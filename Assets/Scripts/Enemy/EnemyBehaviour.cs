﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [Header("Enemy Attribute")]
    public EnemyAttribute enemyAttribute;
    public SoundManager soundManager;
    public GameManager gameManager;

    // Atribute
    [HideInInspector] public bool die = false;
    private bool isBoss;
    [HideInInspector] public int health;
    public Renderer sprite;

    // Animation
    [Header("Animation")]
    public Animator animator;

    // UI
    [Header("Boss UI")]
    public HealthUIScript healthBar;
    public GameObject healthBarUI;

    // color data
    [Header("Enemy Color Data")]
    public Color defaultSpriteColor;
    public Color damagedSpriteColor;

    // Start is called before the first frame update
    void Start()
    {
        health = enemyAttribute.health;
        isBoss = enemyAttribute.isBoss;

        if (isBoss)
        {
            healthBar.SetMaxHealth(health);
            healthBar.SetBossName(gameObject.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void TakeDamage(int damage)
    {
        if (!die)
        {
            StartCoroutine(DamagedSprite());
            health -= damage;

            if (health <= 0)
            {
                if (gameManager != null)
                {
                    if(!gameManager.dialoguePopUp)
                        Die();
                }
                else
                {
                    Die();
                }
            }

            if (isBoss)
            {
                healthBar.SetHealth(health);
            }
        }
    }

    void Die()
    {
        die = true;

        if (isBoss)
        {
            healthBarUI.SetActive(false);
        }

        if (GetComponent<Destroyed>() != null)
        {
            GetComponent<Destroyed>().PlayExplosion();
            StartCoroutine(DestroyObjectCouroutine(0.8f));
        }
        else
        {
            animator.SetTrigger("die");
            StartCoroutine(DestroyObjectCouroutine(0.8f));
        }
    }

    IEnumerator DestroyObjectCouroutine(float time)
    {
        yield return new WaitForSeconds(time);

        if(gameObject.transform.parent != null)
        {
            if (gameObject.transform.parent.gameObject.tag == "Enemy")
            {
                Destroy(gameObject.transform.parent.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        else
        {
            Destroy(gameObject);
        }
        
    }

    IEnumerator DamagedSprite()
    {
        sprite.material.color = damagedSpriteColor;
        yield return new WaitForSeconds(.1f);
        sprite.material.color = defaultSpriteColor;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("DieZone"))
        {
            soundManager.PlayActionSound(3);
            Die();
        }
    }
}
