﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreventObjectOnPosition : MonoBehaviour
{
    public string targetTag = "";

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.gameObject.tag == targetTag)
        {
            Rigidbody2D rb = col.GetComponent<Rigidbody2D>();

            rb.velocity = new Vector2(0f, 0f);
            rb.AddForce(new Vector2(0, 500f));
        }
    }
}
