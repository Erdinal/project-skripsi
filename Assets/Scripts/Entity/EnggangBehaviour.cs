﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnggangBehaviour : MonoBehaviour
{
    [Header("point")]
    public Transform[] point;

    [Header("Attribute")]
    public float speed;
    public float idleTime;
    public Animator animator;

    public int currentIndexPositionPoint;
    public bool reverse;
    private bool canRotate;
    private bool isIdle;
    private float time;
    private int indexToPoint;
    
    void Awake()
    {
        isIdle = true;
        time = 0f;

        if (reverse)
        {
            indexToPoint = -1;
            canRotate = true;
        }
        else
        {
            indexToPoint = 1;
        }
    }

    void Update()
    {
        Rotate();
        Idle();
        Move();
    }

    private void Idle()
    {
        if (isIdle)
        {
            animator.SetBool("isFly", false);

            time += Time.deltaTime;

            if(time >= idleTime)
            {
                isIdle = false;
                time = 0f;
                animator.SetBool("isFly", true);
            }
        }
    }

    private void Move()
    {
        if (!isIdle)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                point[currentIndexPositionPoint + indexToPoint].position, speed * Time.deltaTime);

            if (transform.position == point[currentIndexPositionPoint + indexToPoint].position)
            {
                currentIndexPositionPoint = currentIndexPositionPoint + indexToPoint;

                if (currentIndexPositionPoint > 0 && currentIndexPositionPoint < (point.Length - 1))
                {
                    isIdle = true;
                    if (!reverse)
                    {
                        indexToPoint = 1;
                    }
                    else if (reverse)
                    {
                        indexToPoint = -1;
                    }
                }

                if (currentIndexPositionPoint == 0)
                {
                    canRotate = true;
                    isIdle = true;
                    if (reverse)
                        reverse = false;

                    indexToPoint = 1;
                }

                if (currentIndexPositionPoint == (point.Length - 1))
                {
                    canRotate = true;
                    isIdle = true;
                    if (!reverse)
                        reverse = true;

                    indexToPoint = -1;
                }
            }
        }
    }

    private void Rotate()
    {
        if (canRotate)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            canRotate = false;
        }
    }
}
