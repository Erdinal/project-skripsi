﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PesutBehaviour : MonoBehaviour
{
    [Header("point")]
    public Transform[] point;

    [Header("Attribute")]
    public float speed;
    public Animator animator;

    public bool reverse;
    public int currentIndexPositionPoint;

    private bool canRotate;

    void Awake()
    {
        reverse = true;
        currentIndexPositionPoint = point.Length - 1;
    }

    void Update()
    {
        Rotate();
        Move();
    }

    private void Move()
    {
       
        int indexToPoint = 0;

        if (currentIndexPositionPoint == (point.Length - 1))
        {
            if (!reverse)
            {
                canRotate = true;
            }

            reverse = true;
            indexToPoint = -1;
        }

        if (currentIndexPositionPoint == 0)
        {
            if (reverse)
            {
                canRotate = true;
            }

            reverse = false;
            indexToPoint = 1;
        }

        transform.position = Vector3.MoveTowards(transform.position,
            point[currentIndexPositionPoint + indexToPoint].position, speed * Time.deltaTime);

        if (transform.position == point[currentIndexPositionPoint + indexToPoint].position)
        {
            if (!reverse)
            {
                currentIndexPositionPoint++;
            }

            if (reverse)
            {
                currentIndexPositionPoint--;
            }
        }
    }

    private void Rotate()
    {
        if (canRotate)
        {
            transform.Rotate(new Vector3(0, 180, 0));
            canRotate = false;
        }
    }
}
