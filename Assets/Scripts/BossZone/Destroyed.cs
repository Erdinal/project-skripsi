﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyed : MonoBehaviour
{
    [Header("Explosion Prefabs")]
    public GameObject[] explosions;
    
    public void PlayExplosion()
    {
        foreach (GameObject var in explosions)
        {
            var.SetActive(true);
        }
    }
}
