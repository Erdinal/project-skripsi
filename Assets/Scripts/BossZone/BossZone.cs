﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class BossZone : MonoBehaviour
{
    public GameObject boss;
    public PlayerBehaviour player;
    public GameObject controlMobile;
    public CinemachineConfiner confiner;
    public PolygonCollider2D bossCamera;
    public GameObject[] bossArenaColliders;
    public SceneManagement sceneManagement;

    public bool playerEnterBoss = false;
    private float timeGo = 0f;

    private void Update()
    {
        if(boss == null)
        {
            controlMobile.SetActive(false);

            sceneManagement.BossDefeated(player);

            /*
            timeGo += Time.deltaTime;

            if (timeGo > 3f){
                controlMobile.SetActive(false);
                player.setMoveDir(1);

                if (bossArenaColliders.Length != 0)
                {
                    foreach (GameObject collider in bossArenaColliders)
                    {
                        collider.SetActive(false);
                    }
                }

            }*/
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.name == "Player")
        {
            if (boss != null)
            {
                playerEnterBoss = true;
                boss.SetActive(true);
                player.healthBarBoss.SetActive(true);
            }

            if(bossArenaColliders.Length != 0)
            {
                foreach(GameObject collider in bossArenaColliders)
                {
                    collider.SetActive(true);
                }
            }

            if(confiner != null)
            {
                if(bossCamera != null)
                {
                    confiner.m_BoundingShape2D = bossCamera;
                }
            }
        }
    }
}
