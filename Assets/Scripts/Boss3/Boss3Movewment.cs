﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss3Movewment : MonoBehaviour
{
    EnemyBehaviour enemyBehaviour;

    public Transform playerTransform;
    public int movementSpeed;
    public Animator animator;
    public GameObject attackPoint;
    public LayerMask targetLayer;
    public GameManager gameManager;
    public DialogManager dialogManager;

    public GameObject enemyPrefabs;
    public GameObject summonPoint;
    private bool summonPhaseOne = true;
    private bool summonPhaseTwo = true;

    public float attackInterval;
    private float attackTime = 0f;

    private bool isAttacking = false;
    private float direction = -1;
    private bool facingLeft = true;

    private void Awake()
    {
        enemyBehaviour = GetComponent<EnemyBehaviour>();
    }

    private void Update()
    {
        if (!gameManager.dialoguePopUp)
        {
            Move();
            Attack();
            Summon();
            Die();
        }
    }

    void Move()
    {
        if (direction == -1)
        {
            animator.SetBool("isWalk", true);
        }

        if(direction == 0)
        {
            animator.SetBool("isWalk", false);
        }

        if (!enemyBehaviour.die && !isAttacking)
        {
            if(transform.position.x > playerTransform.position.x)
            {
                if (!facingLeft)
                {
                    Rotate();
                    facingLeft = true;
                }
            }

            if (transform.position.x < playerTransform.position.x)
            {
                if (facingLeft)
                {
                    Rotate();
                    facingLeft = false;
                }
            }

            direction = -1;
        }
        else if (isAttacking)
        {
            animator.SetBool("isWalk", false);
            direction = 0;
        }

        transform.Translate(new Vector3(direction * movementSpeed * Time.deltaTime, 0f, 0f));
    }

    void Attack()
    {
        if (!GetComponent<EnemyBehaviour>().die)
        {
            Collider2D[] col = Physics2D.OverlapCircleAll(attackPoint.transform.position, .5f, targetLayer);

            attackTime += Time.deltaTime;

            if (col.Length > 0)
            {
                foreach (Collider2D collider in col)
                {
                    if (attackTime >= attackInterval)
                    {
                        GetComponent<EnemyBehaviour>().soundManager.PlayActionSound(0);
                        animator.SetTrigger("attack");
                        isAttacking = true;
                        collider.gameObject.GetComponent<PlayerBehaviour>().TakeDamage(10, 0f);
                        attackTime = 0f;
                    }
                }
            }
            else
            {
                isAttacking = false;
            }
        }
    }

    private void Summon()
    {
        if (enemyBehaviour.health < 100 && summonPhaseOne)
        {
            summonPhaseOne = false;
            for(int i = 0; i < 3; i++)
            {
                GameObject summoned = Instantiate(enemyPrefabs, summonPoint.transform.position, 
                    summonPoint.transform.rotation);
                summoned.SetActive(true);
                summoned.transform.parent = null;
                summoned.GetComponent<EnemyBehaviour>().enemyAttribute.lookRadius = 100;

                Debug.Log("Summon 3");
            }
        }

        if(enemyBehaviour.health < 50 && summonPhaseTwo)
        {
            summonPhaseTwo = false;
            for (int i = 0; i < 5; i++)
            {
                GameObject summoned = Instantiate(enemyPrefabs, summonPoint.transform.position,
                    summonPoint.transform.rotation);
                summoned.SetActive(true);
                summoned.transform.parent = null;
                summoned.GetComponent<EnemyBehaviour>().enemyAttribute.lookRadius = 100;

                Debug.Log("Summon 5");
            }
        }
    }

    private void Die()
    {
        if (GetComponent<EnemyBehaviour>().die)
        {
            dialogManager.triggerNextDialog();
            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

            foreach(GameObject enemy in enemies)
            {
                if (enemy.GetComponent<EnemyMovement_1>() != null)
                    Destroy(enemy);
            }
        }
    }

    private void Rotate()
    {
        transform.Rotate(new Vector3(0f, 180, 0f));
    }

    private void OnDrawGizmosSelected() 
    {
        Gizmos.DrawWireSphere(attackPoint.transform.position, .5f);
    }

    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.gameObject.GetComponent<Collider2D>());
        }
    }
}
