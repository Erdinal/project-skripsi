﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIcontrol : MonoBehaviour
{
    public GameObject controlMobileUI;
    public GameObject bossHealthBarUI;
    public GameManager gameManager;
    public PlayerBehaviour playerBehaviour;

    public GameObject[] mobileButtons;

    // Update is called once per frame
    void Update()
    {
        if (gameManager.dialoguePopUp || playerBehaviour.die || 
            gameManager.isPaused || gameManager.levelComplete)
        {
            //controlMobileUI.SetActive(false);
            controlMobileUI.GetComponent<CanvasGroup>().alpha = 0;
            controlMobileUI.GetComponent<CanvasGroup>().interactable = false;

            foreach (GameObject button in mobileButtons)
            {
                button.SetActive(false);
            }

            if(bossHealthBarUI != null)
                bossHealthBarUI.SetActive(false);
        }
        else
        {
            //controlMobileUI.SetActive(true);
            controlMobileUI.GetComponent<CanvasGroup>().alpha = 1;
            controlMobileUI.GetComponent<CanvasGroup>().interactable = true;

            foreach (GameObject button in mobileButtons)
            {
                button.SetActive(true);
            }

            if (bossHealthBarUI != null)
                bossHealthBarUI.SetActive(true);
        }
    }
}
