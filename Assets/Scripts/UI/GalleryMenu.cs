﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GalleryMenu : MonoBehaviour
{
    public PlayerScriptable playerData;

    public GameObject[] itemFlora;
    public GameObject[] itemFauna;

    [Header("None text UI")]
    public GameObject noneTextFlora;
    public GameObject noneTextFauna;

    [Header("Desc Item UI")]
    public GameObject descUI;
    public Image itemImage;
    public Image itemImage2;
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI itemDesc;

    private List<GalleryScriptable> data;
    private int iterationFlora = 0;
    private int iterationFauna = 0;

    private void Awake()
    {
        data = playerData.galleryData;
        ShowData();
    }

    private void ShowData()
    {
        for (int i = 0; i < playerData.itemGalleryUnlocked; i++)
        {
            if(data[i].type == 1)
            {
                Image image = itemFlora[iterationFlora].transform.GetChild(0).GetComponent<Image>();
                TextMeshProUGUI itemName = itemFlora[iterationFlora].transform.GetChild(1).GetComponent<TextMeshProUGUI>();

                itemFlora[iterationFlora].SetActive(true);
                image.sprite = data[i].sprite;
                itemName.text = data[i].name;

                int temp = i;

                itemFlora[iterationFlora].GetComponent<Button>().onClick.AddListener(() => clickListener(temp));

                iterationFlora++;
            }

            if (data[i].type == 2)
            {
                Image image = itemFauna[iterationFauna].transform.GetChild(0).GetComponent<Image>();
                TextMeshProUGUI itemName = itemFauna[iterationFauna].transform.GetChild(1).GetComponent<TextMeshProUGUI>();

                itemFauna[iterationFauna].SetActive(true);
                image.sprite = data[i].sprite;
                itemName.text = data[i].name;

                int temp = i;

                itemFauna[iterationFauna].GetComponent<Button>().onClick.AddListener(() => clickListener(temp));

                iterationFauna++;
            }
        }

        if(iterationFlora > 0)
        {
            noneTextFlora.SetActive(false);
        }

        if (iterationFauna > 0)
        {
            noneTextFauna.SetActive(false);
        }
    }

    void clickListener(int index)
    {
        descUI.SetActive(true);
        itemImage.sprite = data[index].sprite;
        itemImage2.sprite = data[index].image;
        itemName.text = data[index].name;
        itemDesc.text = data[index].description;
    }
}
