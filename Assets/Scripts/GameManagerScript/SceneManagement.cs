﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    [SerializeField] private GameObject levelCompletePanel;
    [SerializeField] private GameObject newGamePropmtUI;

    public GameManager gameManager;
    public SaveManager saveManager;
    public SoundManager soundManager;
    public PlayerScriptable playerData;
    public Animator transitionAnimator;
    private bool once = true;

    [Header("Assign the value for the last level")]
    [SerializeField] private GameObject endGamePanel;

    private void Awake()
    {
        playerData = saveManager.LoadGame();

    }

    public void StartGame()
    {
        if(playerData.indexRespawn > 1)
        {
            newGamePropmtUI.SetActive(true);
        }
        else
        {
            NewGame();
        }
    }

    private void ResetData()
    {
        playerData.indexRespawn = 1;
        playerData.health = 100;
        playerData.coin = 0;
        playerData.med = 0;

        saveManager.SaveGame(playerData);
    }

    public void NewGame()
    {
        ResetData();
        StartCoroutine(LoadLevel(playerData.indexRespawn));
    }


    public void Continue()
    {
        StartCoroutine(LoadLevel(playerData.indexRespawn));
    }

    public void Retry()
    {
        int i = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(LoadLevel(playerData.indexRespawn));
    }

    public void ToMenu()
    {
        gameManager.ContinuePause();
        StartCoroutine(LoadLevel(0));
    }

    public void Exit()
    {
        Application.Quit();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player") && playerData.indexRespawn < 6 && once)
        {
            soundManager.PlayActionSound(6);
            PlayerBehaviour player = col.gameObject.GetComponent<PlayerBehaviour>();
            gameManager.levelComplete = true;

            levelCompletePanel.SetActive(true);
            playerData.indexRespawn++;
            playerData.health = player.health;
            playerData.coin = player.coin;
            playerData.med = player.med;

            once = false;
            saveManager.SaveGame(playerData);
        }
    }

    public void BossDefeated(PlayerBehaviour playerSave)
    {
        if(playerData.indexRespawn < 6 && once)
        {
            soundManager.PlayActionSound(6);
            gameManager.levelComplete = true;

            levelCompletePanel.SetActive(true);
            playerData.indexRespawn++;
            playerData.health = playerSave.health;
            playerData.coin = playerSave.coin;
            playerData.med = playerSave.med;

            once = false;
            saveManager.SaveGame(playerData);
        }
    }

    public void EndGame()
    {
        if(endGamePanel != null)
        {
            endGamePanel.SetActive(true);
            soundManager.PlayActionSound(6);
            ResetData();
        }
    }

    public void ToNextLevel()
    {
        int i = SceneManager.GetActiveScene().buildIndex;
        i++;

        StartCoroutine(LoadLevel(i));
    }

    IEnumerator LoadLevel(int index)
    {
        transitionAnimator.SetTrigger("start");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(index);
    }
}