﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    private GameObject Player;

    [SerializeField]
    private GameObject gameOverPanel;

    public bool isPaused = false;
    public bool dialoguePopUp = false;
    [HideInInspector] public bool levelComplete = false;

    [SerializeField]
    private GameObject pausePanel;

    void Awake()
    {
        Player = GameObject.Find("Player");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }

        if (Player == null)
        {
            gameOverPanel.SetActive(true);
        }
    }

    public void Pause()
    {
        if (!isPaused)
        {
            Time.timeScale = 0;
            isPaused = true;
            pausePanel.SetActive(true);
        }
        else if (isPaused)
        {
            Time.timeScale = 1;
            isPaused = false;
            pausePanel.SetActive(false);
        }
    }

    public void ContinuePause()
    {
        Time.timeScale = 1;
        isPaused = false;
        pausePanel.SetActive(false);
    }
}