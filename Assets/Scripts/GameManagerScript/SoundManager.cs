﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioClip[] audiosMenuUI = null;
    public AudioClip[] audiosAction = null;
    [HideInInspector] public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(int indexSoundClip)
    {
        audioSource.clip = audiosMenuUI[indexSoundClip];
        audioSource.Play();
    }

    public void PlayActionSound(int indexSoundActionClip)
    {
        audioSource.clip = audiosAction[indexSoundActionClip];
        audioSource.Play();
    }
}