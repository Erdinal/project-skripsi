﻿using UnityEngine;

[CreateAssetMenu(fileName = "GalleryData", menuName = "GalleryData", order = 4)]
public class GalleryScriptable : ScriptableObject
{
    public string name;
    public Sprite sprite;
    [TextArea]
    public string description;

    [Header("1.Flora 2.Fauna")]
    [Range(1, 2)]
    public int type;

    public Sprite image;
}