﻿using UnityEngine;

public class SaveManager : MonoBehaviour
{
    public PlayerScriptable playerData;
    public AudioSource soundAudioSource;

    public void SaveGame(PlayerScriptable playerData)
    {
        PlayerPrefs.SetInt("indexRespawn", playerData.indexRespawn);
        PlayerPrefs.SetInt("health", playerData.health);
        PlayerPrefs.SetInt("coin", playerData.coin);
        PlayerPrefs.SetInt("med", playerData.med);
        PlayerPrefs.SetFloat("soundVolume", playerData.soundVolume);
        PlayerPrefs.SetInt("toggleSound", playerData.toogleSound);
        PlayerPrefs.SetInt("itemGallery", playerData.itemGalleryUnlocked);
        PlayerPrefs.Save();
    }

    public PlayerScriptable LoadGame()
    {   

        if (CheckData())
        {
            playerData.indexRespawn = PlayerPrefs.GetInt("indexRespawn");
            playerData.health = PlayerPrefs.GetInt("health");
            playerData.coin = PlayerPrefs.GetInt("coin");
            playerData.med = PlayerPrefs.GetInt("med");
            playerData.soundVolume = PlayerPrefs.GetFloat("soundVolume");
            playerData.toogleSound = PlayerPrefs.GetInt("toggleSound");
            playerData.itemGalleryUnlocked = PlayerPrefs.GetInt("itemGallery");

            if (playerData.toogleSound == 1)
            {
                soundAudioSource.volume = playerData.soundVolume;
            }
            else
            {
                soundAudioSource.volume = 0;
            }

            return playerData;
        }
        else
        {
            playerData.indexRespawn = 1;
            playerData.health = 100;
            playerData.coin = 0;
            playerData.med = 0;

            return playerData;
        }
    }

    private static bool CheckData()
    {
        return PlayerPrefs.HasKey("indexRespawn");
    }
}
