﻿using UnityEngine;
using UnityEngine.UI;

public class AudioSettingsUI : MonoBehaviour
{
    public AudioSource audioSource = null;
    public Slider slider = null;
    public Toggle toggle = null;
    public PlayerScriptable playerData;
    public SaveManager saveManager;

    private void Awake()
    {
        playerData = saveManager.LoadGame();


        slider.value = playerData.soundVolume;

        if (playerData.toogleSound == 1)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
    }

    public void AdjustVolumeUI()
    {
        audioSource.volume = slider.value;
        playerData.soundVolume = slider.value;
        saveManager.SaveGame(playerData);
    }

    public void ToggleSoundUI()
    {
        int enable = 1;

        if (toggle.isOn)
        {
            audioSource.volume = slider.value;
            slider.interactable = true;
            enable = 1;
        }

        if (!toggle.isOn)
        {
            audioSource.volume = 0;
            slider.interactable = false;
            enable = 0;
        }

        playerData.toogleSound = enable;
        saveManager.SaveGame(playerData);
    }
}