﻿using UnityEngine;

public class AudioAmbienceSettings : MonoBehaviour
{
    public PlayerScriptable playerData;
    public AudioSource[] audios;

    void Update()
    {
        if (playerData.toogleSound == 0)
        {
            SettingsAudios(0);
        }
        else
        {
            SettingsAudios(playerData.soundVolume);
        }
    }

    private void SettingsAudios(float volume)
    {
        foreach(AudioSource audio in audios)
        {
            audio.volume = volume;
        }
    }
}