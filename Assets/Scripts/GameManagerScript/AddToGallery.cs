﻿using UnityEngine;

public class AddToGallery : MonoBehaviour
{
    public PlayerScriptable playerData;
    public int unlockGalleryItem;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player" 
            && playerData.itemGalleryUnlocked < unlockGalleryItem)
        {
            playerData.itemGalleryUnlocked = unlockGalleryItem;
        }
    }
}