﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2Movement : MonoBehaviour
{
    [HideInInspector] public int direction = -1;
    public Transform playerTransform;
    public float movementSpeed = 5f;
    private float timerRotate = 0f;
    public float timeRotate = 3f;

    public EnemyBehaviour attribute;

    public BossZone bossZone;
    private bool facingRight = false;
    private bool phaseTwo = false;
    private bool phaseThree = false;

    public GameObject colliderArena2;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (attribute.health < 50)
        {
            if(phaseTwo == false)
            {
                phaseTwo = true;
                movementSpeed++;
            }
        }

        if (attribute.health < 30)
        {
            if (phaseThree == false)
            {
                phaseThree = true;
                movementSpeed++;
            }
        }

        if (attribute.die)
        {
            direction = 0;
        }
        else
        {
            if (bossZone.playerEnterBoss)
            {
                Move();

                if (playerTransform.position.x < transform.position.x && facingRight)
                {
                    Rotate(false);
                }

                if (playerTransform.position.x > transform.position.x && !facingRight)
                {
                    Rotate(true);
                }

                if (attribute.die)
                {
                    colliderArena2.SetActive(false);
                }
            }
        }
    }

    void Move()
    {
        transform.Translate(new Vector3(direction * movementSpeed * Time.deltaTime, 0f, 0f));
    }

    private void Rotate(bool facing)
    {
        timerRotate += Time.deltaTime;

        if (timerRotate > timeRotate)
        {
            direction = -1;
            transform.Rotate(new Vector3(0, 180, 0));
            facingRight = facing;
            timerRotate = 0f;
        }
    }
}
