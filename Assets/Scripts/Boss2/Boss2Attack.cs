﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2Attack : MonoBehaviour
{
    public Boss2Movement boss2Movement;

    private void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameObject player = col.gameObject;

            if (player.GetComponent<PlayerBehaviour>().die == false)
            {
                player.GetComponent<PlayerBehaviour>().TakeDamage(10, 0);
                player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, 100));
            }
            else
            {
                Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.collider);
            }
        }
    }
}
