﻿using System.Collections;
using UnityEngine;

public class FallPlatform : MonoBehaviour
{
    private Rigidbody2D rb;

    public float timeFall;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.name == "Player")
        {
            StartCoroutine(Fall(timeFall));
        }
    }
    
    IEnumerator Fall(float time)
    {
        yield return new WaitForSeconds(timeFall);
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        rb.gravityScale = 1;
    }
}
