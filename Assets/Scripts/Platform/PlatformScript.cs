﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
    [Header("Platform Attribute")]
    public bool vertical;
    public float speed;
    public Transform position_1;
    public Transform position_2;

    private bool traverse = false;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!traverse)
        {
            MovePlatform(1, vertical);
        }

        if (traverse)
        {
            MovePlatform(-1, vertical);
        }
    }

    private void MovePlatform(float direction, bool vertical)
    {
        Vector3 move = Vector3.zero;

        if (!vertical)
        {
            move = new Vector3(direction * speed * Time.deltaTime, 0, 0);
        }

        if (vertical)
        {
            move = new Vector3(0, direction * speed * Time.deltaTime, 0);
        }

        transform.Translate(move);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "PlatformPoint")
        {
            if (traverse)
            {
                traverse = false;
            }
            else
            {
                traverse = true;
            }
        }
    }
}
