﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProjectileBehaviour : MonoBehaviour
{
    private Rigidbody2D rb;
    public float projectileSpeed;
    [HideInInspector] public float xDirection = 0;
    [HideInInspector] public bool move = true;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(new Vector2(xDirection * projectileSpeed, 0));
    }

    // Update is called once per frame
    void Update()
    {
        //if(move)
        //Move(xDirection);
    }

    private void Move(float xDirection)
    {
        //Vector2 move = new Vector2(xDirection * projectileSpeed, rb.velocity.y);

        //rb.velocity = move;

        rb.AddForce(new Vector2(xDirection * projectileSpeed, 0));
        move = false;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemy"))
        {
            if(col.gameObject.GetComponent<EnemyBehaviour>() != null)
                col.gameObject.GetComponent<EnemyBehaviour>().TakeDamage(10);
        }

        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("DieZone"))
        {
            Destroy(gameObject);
        }

        if (col.gameObject.tag.Equals("Enemy"))
        {
            col.gameObject.GetComponent<EnemyBehaviour>().TakeDamage(10);
        }
    }
}
