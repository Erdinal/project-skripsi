﻿using System.Collections;
using UnityEngine;
using TMPro;

public class PlayerBehaviour : MonoBehaviour
{
    // Variabel GameManager
    private GameManager gameManager;
    
    // Variabel Data
    [Header("PlayerData")]
    public PlayerScriptable playerData;
    public SoundManager soundManager;
    
    // Variabel Fisik
    [Header("Player Physic")]
    public Rigidbody2D rb;
    public float speed;
    public float jumpForce;
    private bool isMoving = false;
    private bool onceJump = true;
    public LayerMask layerGround;
    public GameObject groundCheck;
    private Animator animator;

    private bool isGrounded;
    [HideInInspector] public bool facingRight;

    // Variabel Attack
    [Header("Player Melee Attack")]

    [SerializeField] private float attackInterval;
    private float attackTime;
    private bool onceAttack = true;
    private bool isAttacking = false;
    private float attackAnimationTime;
    [SerializeField] private Transform attackPoint;
    [SerializeField] [Range(0.1f, 0.5f)] private float attackRadius;
    [SerializeField] private LayerMask enemyLayerMask;
    [SerializeField] private LayerMask bossLayerMask;

    // Variabel Ranged Attack 
    [Header("Player Ranged Attack")]

    [SerializeField] private float rangedAttackInterval;
    private bool onceRanged = true;
    private bool isRangedAttacking;
    private float rangedAttackTime;
    private float rangedAttackAnimationTime;
    public GameObject projectile;
    [SerializeField] private Transform rangedAttackPoint;

    // Variabel runtime data
    [HideInInspector] public int health;
    [HideInInspector] public int coin = 0;
    [HideInInspector] public int med = 0;
    [HideInInspector] public bool die = false;

    // variabel Damaged
    [Header("Player Color Data")]

    public Renderer spriteRenderer;
    public Color defaultSpriteColor;
    public Color damagedSpriteColor;

    // Variabel UI
    [Header("Player UI")]

    public HealthUIScript healthBar;
    public GameObject healthBarBoss;
    public TextMeshProUGUI coinCountUI;
    public TextMeshProUGUI medCountGUI;

    // Variabel UI mobile control
    private float moveDir;
    private bool hitAttackButton = false;
    private bool hitRangedAttackButton = false;

    [Header("Animator Effect")]
    public Animator medAnimator;

    void Awake()
    {
        gameManager = GameObject.Find("==GAMEMANAGER ==").GetComponent<GameManager>();
        health = playerData.health;
        coin = playerData.coin;
        med = playerData.med;
        coinCountUI.text = coin.ToString();
        medCountGUI.text = med.ToString();
        healthBar.SetMaxHealth(100);
        healthBar.SetHealth(health);
    }

    void Start()
    {
        isGrounded = true;
        animator = GetComponent<Animator>();

        attackTime = 0f;
        attackAnimationTime = 0f;
        isAttacking = false;

        rangedAttackTime = 0f;
        rangedAttackAnimationTime = 0f;
        isRangedAttacking = false;

        facingRight = true;
    }

    void Update()
    {
        
        // check jump
        Collider2D[] col = Physics2D.OverlapCircleAll(groundCheck.transform.position, .1f, 
            layerGround);

        // check moving keyboard
        /*
        if(Input.GetAxis("Horizontal") != 0)
        {
            isMoving = true;
            Move(Input.GetAxis("Horizontal"));
        }
        
        if(Input.GetAxis("Horizontal") == 0)
        {
            isMoving = false;
            Move(0);
        }
        */

        // check grounded
        if (col.Length > 0)
        {
            isGrounded = true;
            animator.SetBool("isGrounded", isGrounded);
        }
        else
        {
            if (!die)
            {
                isGrounded = false;
            }
            else
            {
                isGrounded = true;
            }
            
            animator.SetBool("isGrounded", isGrounded);
        }

        if(gameManager.dialoguePopUp != true)
        {
            Attack();
            RangedAttack();

            // check melee attack
            if (isAttacking || isRangedAttacking)
            {
                Move(0);
            }
            else
            {
                // move with button
                Move(moveDir);
            }

            //medkit
            GetMedKit();

            // jump with keyboard
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }

            // med with keyboard
            if (Input.GetKeyDown(KeyCode.Q))
            {
                UseMed();
            }
        }
        else
        {
            isMoving = false;
            moveDir = 0;
            rb.velocity = new Vector2(0f, rb.velocity.y);
            animator.SetBool("move", false);
        }
    }

    private void RangedAttack()
    {
        if (isGrounded)
        {
            rangedAttackTime += Time.deltaTime;

            if (rangedAttackTime > rangedAttackInterval && !isMoving)
            {
                if (Input.GetKeyDown(KeyCode.E) || hitRangedAttackButton)
                {
                    isRangedAttacking = true;
                    animator.SetTrigger("rangedAttack");
                    soundManager.PlayActionSound(1);
                    GameObject projectileClone = Instantiate(projectile, rangedAttackPoint.transform.position,
                        rangedAttackPoint.transform.rotation);

                    if (facingRight)
                    {
                        projectileClone.GetComponent<PlayerProjectileBehaviour>().xDirection = 1;
                        //projectileClone.GetComponent<PlayerProjectileBehaviour>().move = true;
                    }

                    if (!facingRight)
                    {
                        projectileClone.GetComponent<PlayerProjectileBehaviour>().xDirection = -1;
                        //projectileClone.GetComponent<PlayerProjectileBehaviour>().move = true;
                    }

                    rangedAttackTime = 0f;
                    attackTime = 0f;
                    hitRangedAttackButton = false;
                }
                
            }

            if (isRangedAttacking)
            {
                rangedAttackAnimationTime += Time.deltaTime;
                if (rangedAttackAnimationTime > rangedAttackInterval)
                {
                    isRangedAttacking = false;
                    rangedAttackAnimationTime = 0f;
                }
            }
        }
        else
        {
            hitRangedAttackButton = false;
        }
    }

    private void Attack()
    {
        if (isGrounded)
        {
            attackTime += Time.deltaTime;

            if (attackTime > attackInterval)
            {
                if (Input.GetKeyDown(KeyCode.R) || hitAttackButton)
                {
                    isAttacking = true;
                    animator.SetTrigger("attack");
                    soundManager.PlayActionSound(0);
                    attackTime = 0f;
                    rangedAttackTime = 0f;

                    Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position,
                        attackRadius, enemyLayerMask);

                    foreach (Collider2D enemy in hitEnemies)
                    {
                        if (enemy.GetComponent<EnemyBehaviour>() != null)
                        {
                            enemy.GetComponent<EnemyBehaviour>().TakeDamage(10);
                        }

                        if (enemy.GetComponent<ProjectileBehaviour>() != null)
                        {
                            enemy.GetComponent<ProjectileBehaviour>().Punched();
                        }
                    }

                    hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position,
                        attackRadius, bossLayerMask);

                    foreach (Collider2D enemy in hitEnemies)
                    {
                        if (enemy.GetComponent<EnemyBehaviour>() != null)
                        {
                            enemy.GetComponent<EnemyBehaviour>().TakeDamage(10);
                        }

                        if (enemy.GetComponent<ProjectileBehaviour>() != null)
                        {
                            enemy.GetComponent<ProjectileBehaviour>().Punched();
                        }
                    }

                    hitAttackButton = false;
                }
            }

            if (isAttacking)
            {
                attackAnimationTime += Time.deltaTime;
                if (attackAnimationTime > attackInterval)
                {
                    isAttacking = false;
                    attackAnimationTime = 0f;
                }
            }
        }
        else
        {
            hitAttackButton = false;
        }
    }

    public void Die()
    {
        die = true;
        animator.SetTrigger("die");
        StartCoroutine(DestroyObjectCouroutine(0.8f));
    }

    IEnumerator DestroyObjectCouroutine(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }

    private void Move(float move)
    {
        Vector2 direction = new Vector2(move * speed, rb.velocity.y);

        Rotate(move);

        if (move != 0)
        {
            animator.SetBool("move", true);
        }
        else
        {
            animator.SetBool("move", false);
        }

        rb.velocity = direction;
    }

    public void Jump()
    {
        if (isGrounded && onceJump)
        {
            soundManager.PlayActionSound(2);
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddRelativeForce(new Vector2(0, jumpForce));
            onceJump = false;
        }
        
    }

    public void RealeaseJump()
    {
        onceJump = true;
    }

    // Mobile Control
    public void setMoveDir(float move)
    {
        if (!gameManager.dialoguePopUp)
        {
            moveDir = move;
            if (move != 0)
            {
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }
        }
        else
        {
            moveDir = 0;
            isMoving = false;
        }
    }

    public void TriggerHitAttackButton()
    {
        if(!hitRangedAttackButton && onceAttack)
        {
            hitAttackButton = true;
            onceAttack = false;
        }
    }

    public void ReleaseHitAttackButton()
    {
        onceAttack = true;
    }

    public void TriggerHitRangedAttackButton()
    {
        if (!hitAttackButton && onceRanged)
        {
            hitRangedAttackButton = true;
            onceRanged = false;
        }
    }

    public void ReleaseRangedAttackButton()
    {
        onceRanged = true;
    }

    // Behaviour
    public void TakeDamage(int damage, float knockbackDirection = 0)
    {
        
        health -= damage;
        StartCoroutine(DamagedSprite());
        healthBar.SetHealth(health);

        if (health <= 0)
        {
            Die();
        }
    }

    IEnumerator DamagedSprite()
    {
        spriteRenderer.material.color = damagedSpriteColor;
        yield return new WaitForSeconds(.1f);
        spriteRenderer.material.color = defaultSpriteColor;
    }

    private void Rotate(float move)
    {
        if (facingRight && move < 0)
        {
            facingRight = false;
            transform.Rotate(new Vector3(0, 180, 0));
        }

        if (!facingRight && move > 0)
        {
            facingRight = true;
            transform.Rotate(new Vector3(0, -180, 0));
        }
    }
    public void UseMed()
    {
        if (health < 100 && med > 0)
        {
            soundManager.PlayActionSound(4);
            medAnimator.SetTrigger("useMed");
            health = 100;
            healthBar.SetHealth(health);
            med--;
            medCountGUI.text = med.ToString();
        }
    }

    private void GetMedKit()
    {
        float giveMed = coin % 10;

        if ((giveMed % 10) == 0 && coin > 0)
        {
            med++;
            medCountGUI.text = med.ToString();
            coin = 0;
            coinCountUI.text = coin.ToString();
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("DieZone"))
        {
            soundManager.PlayActionSound(3);
            Die();
        }

        if (col.gameObject.tag.Equals("Collectible"))
        {
            soundManager.PlayActionSound(5);
            coin++;
            coinCountUI.text = coin.ToString();
            Destroy(col.gameObject);
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Platform"))
        {
            transform.SetParent(col.gameObject.GetComponent<Transform>());
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Platform"))
        {
            transform.SetParent(null);
        }
    }
}
