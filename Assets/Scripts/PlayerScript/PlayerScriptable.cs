﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "PlayerData", order = 1)]
public class PlayerScriptable : ScriptableObject
{
    public int health;
    public int med;
    public int coin;

    public int itemGalleryUnlocked;
    public List<GalleryScriptable> galleryData;
    public int indexRespawn;

    [Header("Sound Settings")]
    [Range(0, 1f)] public float soundVolume;
    public int toogleSound;
}
